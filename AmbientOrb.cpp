/*
 * AmbientOrb.cpp
 *
 * Arduino Library for Ambient Markup Language
 * The API used to interface to Ambient Devices' Orb
 * notification device (from 2003 or so). For info see
 * www.ambientdevices.com.
 *
 * Arduino Library
 * (c) 2013 Joe George, jgeorge@nbi.com
 *
 * API glommed from Ambient Device's original API documentation
 * API written by Benjamin Resner benres@ambientdevices.com
 * (c) 2003 Ambient Devices, Inc.
 * (unsure what license code samples were published under)
 *
 * See LICENSE for license information (MIT License)
 *
 */

#include <AmbientOrb.h>
#include <avr/pgmspace.h>
#include <SoftwareSerial.h>

// FIXME see if this helps - this should only include this code for
// Arduinos big enough to hold the code in RAM
#ifndef ARDUINO_TINY
const char* AmbientOrb::ANIM_NAME[NUM_ANIMS] = {
  "none",        // 0
  "very slow",   // 1
  "slow",        // 2
  "medium slow", // 3
  "medium",      // 4
  "medium fast", // 5
  "fast",        // 6
  "very fast",   // 7
  "cresendo",    // 8
  "heartbeat"    // 9
};

const char* AmbientOrb::COLOR_NAME[NUM_COLORS] = {
  "red",            // 0
  "light red",      // 1
  "dark orange",    // 2
  "orange",         // 3
  "light orange",   // 4
  "dark yellow",    // 5
  "yellow",         // 6
  "lime green",     // 7
  "pale green",     // 8
  "green -3",       // 9
  "green -2",       // 10
  "green -1",       // 11
  "green",          // 12
  "green + 1",      // 13
  "green + 2",      // 14
  "pale aqua",      // 15
  "aqua",           // 16
  "dark aqua",      // 17
  "cyan",           // 18
  "dark cyan",      // 19
  "light blue",     // 20
  "sky blue",       // 21
  "blue -2",        // 22
  "blue -1",        // 23
  "blue",           // 24
  "deep blue",      // 25
  "very deep blue", // 26
  "violet",         // 27
  "purple",         // 28
  "light purple",   // 29
  "magenta",        // 30
  "magenta +1",     // 31
  "magenta +2",     // 32
  "magenta +3",     // 33
  "magenta +4",     // 34
  "magenta +5",     // 35
  "white"           // 36
};
#endif

SoftwareSerial* orbSerial;
#ifndef ARDUINO_TINY
struct OrbInfo orbInfo;
#endif

// constructor
AmbientOrb::AmbientOrb() {
#ifndef ARDUINO_TINY
  // dummy data
  orbInfo.valid=false;
  orbInfo.productID=4226;
  orbInfo.versionID=4116;
  orbInfo.serialNumber="XXXYYYZZZ";
  orbInfo.brightness=HIGH;
  orbInfo.ignorePagerData=false;
  orbInfo.premiumCapcode=false;
  orbInfo.copyrightMessage="(c)2003 Amb Dev, Orb 2.0b";
  orbInfo.disableButtons=false;
  orbInfo.brightnessButtonState=false;
  orbInfo.resetButtonState=false;
#endif
}

void AmbientOrb::begin(int rxpin, int txpin) {
  orbSerial = new SoftwareSerial(rxpin, txpin);
  orbSerial->begin(19200);
}

#ifndef ARDUINO_TINY
// get info block
struct OrbInfo AmbientOrb::getInfo() {
  orbSerial->flush();
  orbSerial->print(AML_COMMAND);
  orbSerial->println(AML_GETINFO);
  // give the orb a few ms to process the request 
  delay(5);
  orbInfo.valid=false;
  if (orbSerial->available() > 28 && orbSerial->read() == '!' && orbSerial->read() == AML_GETINFO) {
    orbInfo.valid = true;
  } else {
      // return dummy data if we don't have anything else
      return orbInfo;
  }

  orbInfo.productID = orbSerial->read() * 256 + orbSerial->read();
  orbInfo.versionID = orbSerial->read() * 256 + orbSerial->read();

  char serial[10];
  for (int i=0; i<9; i++ ) {
    serial[i] = (char)orbSerial->read();
  }
  serial[9] = 0; // null terminate
  orbInfo.serialNumber = serial;

  orbInfo.channelOffset = orbSerial->read();

  long temp = 0;
  for (int i=0; i<6; i++) {
    temp *= 256;
    temp += orbSerial->read();
  }
  orbInfo.channelID = temp;

  orbInfo.brightness = orbSerial->read();

  // ignore pages received (not implemented)
  orbSerial->read();

  orbInfo.ignorePagerData = ((char)orbSerial->read() == 'T');

  orbInfo.premiumCapcode = ((char)orbSerial->read() == 'T');

  // mystery bytes
  orbSerial->read();
  orbSerial->read();
 
  char copyright[50];
  char c = -1;
  int i=0;
  while (c != 0 && i < 50) {
    c = (char)orbSerial->read();
    copyright[i++] = c;
  }
  copyright[i] = 0; //null terminate
  orbInfo.copyrightMessage = copyright;

  return orbInfo;
}
#endif

// send raw RGB data
void AmbientOrb::setRGB(byte red, byte green, byte blue) {
  orbSerial->flush();
  if (red < 0 || red > 176) return;
  if (green < 0 || green > 176) return;
  if (blue < 0 || blue > 176) return;
  orbSerial->print(AML_COMMAND);
  orbSerial->print(AML_SETRGB);
  orbSerial->print((char)red);
  orbSerial->print((char)green);
  orbSerial->print((char)blue);
  // we need 5ms for the Orb to process, so we don't overrun it with rapid requests
  delay(5);
}

// send "formatted" data
void AmbientOrb::setColor(int color, int animation) {
  orbSerial->flush();

  char a,b;
  a = (color + (37 * animation)) / 94 + 32;
  b = (color + (37 * animation)) % 94 + 32;
  orbSerial->print(AML_COMMAND);
  orbSerial->print(AML_SETCOLOR);
  orbSerial->print(a);
  orbSerial->println(b);
}

// ping the orb to make sure it's responsive
bool AmbientOrb::ping() {
  orbSerial->flush();
  orbSerial->print(AML_COMMAND);
  orbSerial->println(AML_PING);
  // give the orb a few ms to process the request 
  delay(5);
  if (orbSerial->available() > 1) {
    if (orbSerial->read() == AML_PING && orbSerial->read() == AML_SUCCESS) {
      return true;
    }
  }
  return false;
}

#ifndef ARDUINO_TINY
// ignore data coming from pager
void AmbientOrb::ignorePagerData(bool ignore) {
  orbSerial->flush();
  orbSerial->print(AML_COMMAND);
  orbSerial->print(AML_IGNOREPAGER);
  orbSerial->println((ignore ? "T" : "F"));
  orbInfo.ignorePagerData = (ignore ? "T" : "F");
}

// ignore automatic button actions
void AmbientOrb::disableButtons(bool disable) {
  orbSerial->flush();
  orbSerial->print(AML_COMMAND);
  orbSerial->print(AML_LOCALBUTTON);
  orbSerial->println((disable ? "E" : "I"));
  orbInfo.disableButtons = (disable ? "E" : "I");
}
#endif

// set brightless level
void AmbientOrb::setBrightness(int level) {
  orbSerial->flush();
  orbSerial->print(AML_COMMAND);
  orbSerial->print(AML_BRIGHTNESS);
  orbSerial->println(level, DEC);
#ifndef ARDUINO_TINY
  orbInfo.brightness = level;
#endif
}

#ifndef ARDUINO_TINY
int getBrightness() {
  return orbInfo.brightness;
}

// get name for color value
const char* AmbientOrb::getColorName(int color) {
  return AmbientOrb::COLOR_NAME[color];
}

// get name for animation value
const char* AmbientOrb::getAnimName(int anim) {
  return AmbientOrb::ANIM_NAME[anim];
}
#endif
/* end ORB.cpp */
