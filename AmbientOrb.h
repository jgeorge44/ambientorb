/*
 * AmbientOrb.h
 *
 * Arduino Library for Ambient Markup Language
 * The API used to interface to Ambient Devices' Orb
 * notification device (from 2003 or so). For info see
 * www.ambientdevices.com.
 *
 * Arduino Library
 * (c) 2013 Joe George, jgeorge@nbi.com
 *
 * API glommed from Ambient Device's original API documentation
 * API written by Benjamin Resner benres@ambientdevices.com
 * (c) 2003 Ambient Devices, Inc.
 * (unsure what license code samples were published under)
 *
 * See LICENSE for license information (MIT License)
 *
 */

#ifndef _AmbientOrb_H_
#define _AmbientOrb_H_

#include <Arduino.h>

#define ARDUINO_TINY true 

/* Animation definitions */
#define	NUM_ANIMS	10

#define	NONE		0
#define	VSLOW		1
#define	SLOW		2
#define	MEDSLOW		3
#define	MED		4
#define	MED_FAST	5
#define	FAST		6
#define	VFAST		7
#define	CRESCENDO	8
#define	HEARTBEAT	9

/* Colors by name */
#define	NUM_COLORS	37

#define	RED      	0
#define	LIGHT_RED	1
#define	DARK_ORANGE	2
#define	ORANGE    	3
#define	LIGHT_ORANGE	4
#define	DARK_YELLOW	5
#define	YELLOW    	6
#define	PALE_GREEN	8
#define	GREEN_MINUS_3	9
#define	GREEN_MINUS_2	10
#define	GREEN_MINUS_1	11
#define	GREEN    	12
#define	GREEN_PLUS_1	13
#define	GREEN_PLUS_2	14
#define	PALE_AQUA	15
#define	AQUA    	16
#define	DARK_AQUA	17
#define	CYAN    	18
#define	DARK_CYAN	19
#define	LIGHT_BLUE	20
#define	SKY_BLUE	21
#define	BLUE_MINUS_2	22
#define	BLUE_MINUS_1	23
#define	BLUE    	24
#define	DEEP_BLUE	25
#define	VERY_DEEP_BLUE	26
#define	VIOLET  	27
#define	PURPLE	        28
#define	LIGHT_PURPLE	29
#define	MAGENTA	        30
#define	MAGENTA_PLUS_1	31
#define	MAGENTA_PLUS_2	32
#define	MAGENTA_PLUS_3	33
#define	MAGENTA_PLUS_4	34
#define	MAGENTA_PLUS_5	35
#define	WHITE	        36

#ifndef ARDUINO_TINY
struct OrbInfo {
  bool valid;
  int productID;
  int versionID;
  char* serialNumber;
  int brightness;
  bool ignorePagerData;
  bool premiumCapcode;
  char* copyrightMessage;
  long channelID;
  int channelOffset;
  bool channelShared;
  bool disableButtons;
  bool brightnessButtonState;
  bool resetButtonState;
};
#endif
  

/* Brightness levels */
#define	DIM	0
#define	MED	1
#define	HIGH	2

/* Button States */
#ifndef ARDUINO_TINY
#define RESET_DOWN      200
#define RESET_UP        201
#define	BRIGHT_DOWN     202
#define BRIGHT_UP       203
#endif
/* AML commands */
#define AML_COMMAND     '~'
#define AML_SETCOLOR    'A'
#define AML_SETRGB      'D'
#ifndef ARDUINO_TINY
#define AML_IGNOREPAGER 'G'
#define AML_GETINFO     'I'
#define AML_LOCALBUTTON 'L'
#endif
#define AML_PING        'P'
#define AML_BRIGHTNESS  'R'
#define AML_SUCCESS     '+'
#define AML_FAILURE     '-'

class AmbientOrb {
        public:
        
            // constructor
            AmbientOrb();

            // initializer
            void begin(int rxpin, int txpin);
            
#ifndef ARDUINO_TINY
            // get info block
            struct OrbInfo getInfo();
#endif
            
            // send raw RGB data
            void setRGB(byte red, byte green, byte blue);
            
            // send "formatted" data
            void setColor(int color, int animation);
            
            // ping the orb to make sure it's responsive
            bool ping();
            
#ifndef ARDUINO_TINY
            // ignore data coming from pager
            void ignorePagerData(bool ignore);
            
            // ignore automatic button actions
            void disableButtons(bool disable);
#endif
            
            // set brightless level
            void setBrightness(int level);
            
#ifndef ARDUINO_TINY
            // get brightless level
            int getBrightness();
            
            // get name for color value
            const char* getColorName(int color);

            // get name for animation value
            const char* getAnimName(int anim);
            
            // arrays
            static const char* ANIM_NAME[NUM_ANIMS];
            static const char* COLOR_NAME[NUM_COLORS];
#endif
};


#endif // _AmbientOrb_H_


